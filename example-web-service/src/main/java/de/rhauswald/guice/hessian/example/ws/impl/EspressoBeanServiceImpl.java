package de.rhauswald.guice.hessian.example.ws.impl;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.example.api.espressobeans.DtoEspressoBean;
import de.rhauswald.guice.hessian.example.api.espressobeans.EspressoBeanService;

import java.util.Arrays;
import java.util.List;

@HessianWebService(urlPattern = "/espresso")
public class EspressoBeanServiceImpl implements EspressoBeanService {
	@Override
	public List<DtoEspressoBean> list() {
		return Arrays.asList(
				new DtoEspressoBean("kaffee-manu-faktur - Espresso Emilio"),
				new DtoEspressoBean("Mokito - Rosso")
		);
	}
}
