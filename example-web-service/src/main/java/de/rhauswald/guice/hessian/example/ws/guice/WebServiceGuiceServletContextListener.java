package de.rhauswald.guice.hessian.example.ws.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import de.rhauswald.guice.hessian.HessianWebServicesModule;
import de.rhauswald.guice.hessian.UrlPrefix;

public class WebServiceGuiceServletContextListener extends GuiceServletContextListener {
	@Override
	protected Injector getInjector() {
		return Guice.createInjector(
				new HessianWebServicesModule(UrlPrefix.forPrefix("/hessian/"), "de.rhauswald.guice.hessian.example.ws.impl")
		);
	}
}
