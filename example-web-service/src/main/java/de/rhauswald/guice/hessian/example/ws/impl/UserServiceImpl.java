package de.rhauswald.guice.hessian.example.ws.impl;


import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.example.api.users.DtoUser;
import de.rhauswald.guice.hessian.example.api.users.UserService;

import java.util.Arrays;
import java.util.List;

@HessianWebService
public class UserServiceImpl implements UserService {
	@Override
	public List<DtoUser> list() {
		return Arrays.asList(
				new DtoUser("Chuck Norris", "chuck@norris.de"),
				new DtoUser("Jean Luc Picard", "jean-luc@picard.de")
		);
	}
}
