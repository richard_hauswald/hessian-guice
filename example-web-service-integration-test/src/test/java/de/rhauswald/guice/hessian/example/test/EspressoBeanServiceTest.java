package de.rhauswald.guice.hessian.example.test;

import com.caucho.hessian.client.HessianProxyFactory;
import de.rhauswald.guice.hessian.example.api.espressobeans.DtoEspressoBean;
import de.rhauswald.guice.hessian.example.api.espressobeans.EspressoBeanService;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class EspressoBeanServiceTest {
	@Test
	public void testHello() throws Exception {
		final String urlName = "http://localhost:31337/hessian/espresso";
		final Class<EspressoBeanService> api = EspressoBeanService.class;
		final EspressoBeanService service = (EspressoBeanService) new HessianProxyFactory().create(api, urlName);
		final List<DtoEspressoBean> list = service.list();
		assertThat(list, is(notNullValue()));
		assertThat(list.size(), is(2));
		DtoEspressoBean dtoEspressoBean = list.get(0);
		assertThat(dtoEspressoBean, is(notNullValue()));
		assertThat(dtoEspressoBean.getName(), is("kaffee-manu-faktur - Espresso Emilio"));
		dtoEspressoBean = list.get(1);
		assertThat(dtoEspressoBean, is(notNullValue()));
		assertThat(dtoEspressoBean.getName(), is("Mokito - Rosso"));
	}
}
