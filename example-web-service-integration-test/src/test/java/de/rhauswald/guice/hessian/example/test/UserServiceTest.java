package de.rhauswald.guice.hessian.example.test;

import com.caucho.hessian.client.HessianProxyFactory;
import de.rhauswald.guice.hessian.example.api.users.DtoUser;
import de.rhauswald.guice.hessian.example.api.users.UserService;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class UserServiceTest {
	@Test
	public void testHello() throws Exception {
		final Class<UserService> api = UserService.class;
		final String urlName = "http://localhost:31337/hessian/UserService";
		final UserService service = (UserService) new HessianProxyFactory().create(api, urlName);
		final List<DtoUser> list = service.list();
		assertThat(list, is(notNullValue()));
		assertThat(list.size(), is(2));
		DtoUser dtoUser = list.get(0);
		assertThat(dtoUser, is(notNullValue()));
		assertThat(dtoUser.getName(), is("Chuck Norris"));
		assertThat(dtoUser.getEmail(), is("chuck@norris.de"));
		dtoUser = list.get(1);
		assertThat(dtoUser, is(notNullValue()));
		assertThat(dtoUser.getName(), is("Jean Luc Picard"));
		assertThat(dtoUser.getEmail(), is("jean-luc@picard.de"));
	}
}
