package de.rhauswald.guice.hessian;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(ElementType.TYPE)
@Retention(RUNTIME)
public @interface HessianWebService {
	String urlPattern() default "";
	Class<?> apiClazz() default Object.class;
	boolean bindApiClazzToAnnotatedInstance() default true;
}
