package de.rhauswald.guice.hessian;

public interface HessianKeyBindingBuilder {
	void usingUrl(String url);
}
