package de.rhauswald.guice.hessian.internal;

import com.google.inject.Binder;
import com.google.inject.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Proxy;

public class ScopedInstanceProxyFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScopedInstanceProxyFactory.class);
	private final Binder binder;

	public ScopedInstanceProxyFactory(Binder binder) {
		this.binder = binder;
	}

	@SuppressWarnings("unchecked")
	public <T> T forKey(Class<T> instanceInterfaceClass, Key<? extends T> key) {
		if (!instanceInterfaceClass.isInterface()) {
			final String message = "This class is using " + Proxy.class + " which requires the proxied instance " +
					"to implement an interface defining all the methods which should be proxied.";
			throw new IllegalArgumentException(message);
		}
		LOGGER.debug("Creating scoped instance proxy implementing {} for for key {}", instanceInterfaceClass, key);
		final ScopedInstanceProxy<? extends T> invocationHandler = new ScopedInstanceProxy<>(key);
		binder.requestInjection(invocationHandler);

		final ClassLoader classLoader = instanceInterfaceClass.getClassLoader();
		final Class[] interfaces = {instanceInterfaceClass};
		final Object scopeIndependentServiceInstanceProxy = Proxy.newProxyInstance(classLoader, interfaces, invocationHandler);

		return (T) scopeIndependentServiceInstanceProxy;
	}
}
