package de.rhauswald.guice.hessian.internal;

import com.google.inject.Key;
import de.rhauswald.guice.hessian.HessianKeyBindingBuilder;

public class HessianKeyBindingBuilderImpl<T> implements HessianKeyBindingBuilder {
	private final Class<T> apiClazz;
	private final Key<? extends T> key;
	private String url;

	public HessianKeyBindingBuilderImpl(Class<T> apiClazz, Key<? extends T> key) {
		this.apiClazz = apiClazz;
		this.key = key;
	}

	@Override
	public void usingUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public Class<?> getApiClazz() {
		return apiClazz;
	}

	public Key<? extends T> getKey() {
		return key;
	}
}
