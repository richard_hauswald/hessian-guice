package de.rhauswald.guice.hessian;

import com.caucho.hessian.server.HessianServlet;
import com.google.inject.Key;
import com.google.inject.servlet.ServletModule;
import de.rhauswald.guice.hessian.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class HessianWebServicesModule extends ServletModule {
	private static final Logger LOGGER = LoggerFactory.getLogger(HessianWebServicesModule.class);

	private final AnnotationBasedHessianWebServiceConfigurationProvider configurationProvider;
	private final String[] serviceImplementationsBasePackages;
	private final HessianServletFactory hessianServletFactory;
	private final Set<HessianKeyBindingBuilderImpl<?>> hessianKeyBindingBuilders;

	/**
	 * Call with serviceImplementationsBasePackages to enable annotation auto configuration. Example Usage:
	 * <p>Export all classes annotated with {@link de.rhauswald.guice.hessian.HessianWebService}
	 * under package "de.rhauswald.guice.hessian.example.ws.impl" as hessian web service with the url prefix
	 * "/hessian/":
	 * <pre>
	 * Guice.createInjector(...,
	 * 		new HessianWebServicesModule(
	 * 			UrlPrefix.forPrefix("/hessian/"),
	 * 			"de.rhauswald.guice.hessian.example.ws.impl"),
	 * 		...
	 * 	);
	 * </pre>
	 * </p>
	 */
	public HessianWebServicesModule(UrlPrefix urlPrefix, String... serviceImplementationsBasePackages) {
		super();
		this.serviceImplementationsBasePackages = serviceImplementationsBasePackages;
		this.configurationProvider = new AnnotationBasedHessianWebServiceConfigurationProvider(urlPrefix);
		this.hessianServletFactory = new HessianServletFactory();
		this.hessianKeyBindingBuilders = new HashSet<>();
	}

	/**
	 * Call with serviceImplementationsBasePackages to enable annotation auto configuration. Example Usage:
	 * <p>Export all classes annotated with {@link de.rhauswald.guice.hessian.HessianWebService}
	 * under package "de.rhauswald.guice.hessian.example.ws.impl" as hessian web service without a url prefix:
	 * <pre>
	 * Guice.createInjector(...,
	 * 		new HessianWebServicesModule(
	 * 			"de.rhauswald.guice.hessian.example.ws.impl"),
	 * 		...
	 * 	);
	 * </pre>
	 * </p>
	 */
	public HessianWebServicesModule(String... serviceImplementationsBasePackages) {
		super();
		this.serviceImplementationsBasePackages = serviceImplementationsBasePackages;
		this.configurationProvider = new AnnotationBasedHessianWebServiceConfigurationProvider(UrlPrefix.none());
		this.hessianServletFactory = new HessianServletFactory();
		this.hessianKeyBindingBuilders = new HashSet<>();
	}

	/**
	 * Implement to manually configure hessian web services. Example usage:
	 * <pre>
	 *   Guice.createInjector(..., new HessianWebServicesModule() {
	 *
	 *     {@literal @}Override
	 *     protected void configureHessianWebServices() {
	 *       <b>serveHessianWebService(UserService.class).usingUrl("/UserService")</b>
	 *     }
	 *   }
	 * </pre>
	 */
	protected void configureHessianWebServices() {
	}

	protected final <T> HessianKeyBindingBuilder serveHessianWebService(Class<T> apiClazz) {
		final Key<T> key = Key.get(apiClazz);
		final HessianKeyBindingBuilderImpl<T> builder = new HessianKeyBindingBuilderImpl<>(apiClazz, key);
		hessianKeyBindingBuilders.add(builder);
		return builder;
	}

	protected final <T> HessianKeyBindingBuilder serveHessianWebService(Class<T> apiClazz, Key<? extends T> instanceKey) {
		final HessianKeyBindingBuilderImpl<T> builder = new HessianKeyBindingBuilderImpl<>(apiClazz, instanceKey);
		hessianKeyBindingBuilders.add(builder);
		return builder;
	}

	@Override
	@SuppressWarnings("unchecked")
	protected final void configureServlets() {
		LOGGER.trace("Configuring hessian web service servlets");
		ScopedInstanceProxyFactory scopedInstanceProxyFactory = new ScopedInstanceProxyFactory(binder());

		configureHessianWebServices();
		autoConfigureHessianWebServices();

		for (HessianKeyBindingBuilderImpl hessianKeyBindingBuilder : hessianKeyBindingBuilders) {
			final Class<?> apiClazz = hessianKeyBindingBuilder.getApiClazz();
			final String url = hessianKeyBindingBuilder.getUrl();
			final Object scopedInstanceProxy = scopedInstanceProxyFactory.forKey(apiClazz, hessianKeyBindingBuilder.getKey());
			final HessianServlet hessianServlet = hessianServletFactory.hessianServlet(apiClazz, scopedInstanceProxy);

			LOGGER.info("Serving {} using url {}", apiClazz, url);
			serve(url).with(hessianServlet);
		}
	}

	@SuppressWarnings("unchecked")
	protected final void autoConfigureHessianWebServices() {
		if (serviceImplementationsBasePackages != null) {
			for (String packageName : serviceImplementationsBasePackages) {
				final Set<HessianWebServiceConfiguration> configurations = configurationProvider.provideConfigurations(packageName);
				for (HessianWebServiceConfiguration configuration : configurations) {
					final Class<?> apiClazz = configuration.getApiClazz();
					final String urlPattern = configuration.getUrlPattern();

					if(configuration.isBindApiClazzToAnnotatedInstance()) {
						bind(apiClazz).to(configuration.getAnnotatedApiImplClazz());
					}
					serveHessianWebService(apiClazz).usingUrl(urlPattern);
				}
			}
		}
	}
}
