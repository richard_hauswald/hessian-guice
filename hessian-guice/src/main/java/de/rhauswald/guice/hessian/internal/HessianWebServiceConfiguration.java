package de.rhauswald.guice.hessian.internal;

public class HessianWebServiceConfiguration {
	private final String urlPattern;
	private final Class apiClazz;
	private final Class annotatedApiImplClazz;
	private final boolean bindApiClazzToAnnotatedInstance;

	public HessianWebServiceConfiguration(String urlPattern, Class apiClazz, Class annotatedApiImplClazz, boolean bindApiClazzToAnnotatedInstance) {
		this.urlPattern = urlPattern;
		this.apiClazz = apiClazz;
		this.annotatedApiImplClazz = annotatedApiImplClazz;
		this.bindApiClazzToAnnotatedInstance = bindApiClazzToAnnotatedInstance;
	}

	public String getUrlPattern() {
		return urlPattern;
	}

	public Class getApiClazz() {
		return apiClazz;
	}

	public Class getAnnotatedApiImplClazz() {
		return annotatedApiImplClazz;
	}

	public boolean isBindApiClazzToAnnotatedInstance() {
		return bindApiClazzToAnnotatedInstance;
	}

	@Override
	public String toString() {
		return "HessianWebServiceConfiguration{" +
				"urlPattern='" + urlPattern + '\'' +
				", apiClazz=" + apiClazz +
				", annotatedApiImplClazz=" + annotatedApiImplClazz +
				", bindApiClazzToAnnotatedInstance=" + bindApiClazzToAnnotatedInstance +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		HessianWebServiceConfiguration that = (HessianWebServiceConfiguration) o;

		if (bindApiClazzToAnnotatedInstance != that.bindApiClazzToAnnotatedInstance) return false;
		if (annotatedApiImplClazz != null ? !annotatedApiImplClazz.equals(that.annotatedApiImplClazz) : that.annotatedApiImplClazz != null)
			return false;
		if (apiClazz != null ? !apiClazz.equals(that.apiClazz) : that.apiClazz != null) return false;
		if (urlPattern != null ? !urlPattern.equals(that.urlPattern) : that.urlPattern != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = urlPattern != null ? urlPattern.hashCode() : 0;
		result = 31 * result + (apiClazz != null ? apiClazz.hashCode() : 0);
		result = 31 * result + (annotatedApiImplClazz != null ? annotatedApiImplClazz.hashCode() : 0);
		result = 31 * result + (bindApiClazzToAnnotatedInstance ? 1 : 0);
		return result;
	}
}
