package de.rhauswald.guice.hessian.internal;

import com.google.inject.Injector;
import com.google.inject.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ScopedInstanceProxy<T> implements InvocationHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScopedInstanceProxy.class);
	private final Key<T> key;

	@Inject
	private Injector injector;

	public ScopedInstanceProxy(Key<T> key) {
		this.key = key;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		final Object instance;
		try {
			LOGGER.debug("Requesting instance for {} from hessian injector", key);

			instance = injector.getInstance(key);

			LOGGER.debug("Got {} as instance of {} from hessian injector", instance, key);
		} catch (Exception e) {
			LOGGER.error("Could not get an instance for key " + key, e);
			throw new UnsupportedOperationException("The servers di framework is not configured properly");
		}

		LOGGER.trace("Invoking {} on Hessian web service implementation {}", method, instance);
		return method.invoke(instance, args);
	}
}
