package de.rhauswald.guice.hessian.internal;

import com.caucho.hessian.server.HessianServlet;

public class HessianServletFactory {
	public HessianServlet hessianServlet(Class<?> apiClazz, Object delegationInstance) {
		if (!apiClazz.isInterface()) {
			throw new IllegalArgumentException("apiClazz must be an interface");
		}
		if (!apiClazz.isAssignableFrom(delegationInstance.getClass())) {
			throw new IllegalArgumentException("delegationInstance must implement apiClazz");
		}

		final HessianServlet hessianServlet = new HessianServlet();
		hessianServlet.setHomeAPI(apiClazz);
		hessianServlet.setHome(delegationInstance);

		return hessianServlet;
	}
}
