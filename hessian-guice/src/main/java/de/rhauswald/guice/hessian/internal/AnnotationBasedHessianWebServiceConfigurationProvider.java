package de.rhauswald.guice.hessian.internal;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.UrlPrefix;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class AnnotationBasedHessianWebServiceConfigurationProvider {
	private static final Logger LOGGER = LoggerFactory.getLogger(AnnotationBasedHessianWebServiceConfigurationProvider.class);
	private final UrlPrefix defaultUrlPrefix;

	public AnnotationBasedHessianWebServiceConfigurationProvider(UrlPrefix defaultUrlPrefix) {
		this.defaultUrlPrefix = defaultUrlPrefix;
	}

	public Set<HessianWebServiceConfiguration> provideConfigurations(String serviceImplementationsBasePackage) {
		LOGGER.trace("Scanning {} for hessian web service implementations", serviceImplementationsBasePackage);
		final Set<Class<?>> hessianWebServiceClasses = hessianWebServiceClasses(serviceImplementationsBasePackage);
		LOGGER.debug("Found the following hessian web service implementations: {}", hessianWebServiceClasses);

		Set<HessianWebServiceConfiguration> configurations = new HashSet<>();
		for (Class<?> hessianWebServiceClazz : hessianWebServiceClasses) {
			LOGGER.trace("Creating configuration for hessian web service implementation {}", hessianWebServiceClazz);
			final HessianWebService hessianWebServiceAnnotation = hessianWebServiceClazz.getAnnotation(HessianWebService.class);
			final String hessianWebServiceAnnotationUrlPattern = hessianWebServiceAnnotation.urlPattern();
			final Class<?> hessianWebServiceAnnotationApiClazz = hessianWebServiceAnnotation.apiClazz();
			final boolean bindApiClazzToAnnotatedInstance = hessianWebServiceAnnotation.bindApiClazzToAnnotatedInstance();

			final Class<?> apiClazz;
			if (hessianWebServiceAnnotationApiClazz != Object.class) {
				apiClazz = hessianWebServiceAnnotationApiClazz;
			} else {
				final Class[] interfaces = hessianWebServiceClazz.getInterfaces();
				if (interfaces.length == 1) {
					apiClazz = interfaces[0];
				} else if (interfaces.length > 1) {
					final String message = "Could not determine api interface class of " + hessianWebServiceClazz +
							" because it implements more than one interface class. Please use @" +
							HessianWebService.class.getCanonicalName() + ".apiClazz to indicate the one that should " +
							"be exposed as Hessian web service.";
					throw new UnsupportedOperationException(message);
				} else {
					final String message = hessianWebServiceClazz + " does not implement any interface. Please " +
							"extract an interface from this class and try again";
					throw new UnsupportedOperationException(message);
				}
			}

			final String urlPrefix = defaultUrlPrefix.getPrefix();
			final String urlPattern = urlPattern(hessianWebServiceAnnotationUrlPattern, apiClazz, urlPrefix);
			final HessianWebServiceConfiguration configuration = new HessianWebServiceConfiguration(
					urlPattern,
					apiClazz,
					hessianWebServiceClazz,
					bindApiClazzToAnnotatedInstance);
			LOGGER.debug("Created configuration {} for hessian web service implementation {}", configuration, hessianWebServiceClazz);
			configurations.add(configuration);
		}

		return configurations;
	}

	private Set<Class<?>> hessianWebServiceClasses(String serviceImplementationsBasePackage) {
		Reflections reflections = new Reflections(serviceImplementationsBasePackage);
		final Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(HessianWebService.class);
		return typesAnnotatedWith;
	}

	private String urlPattern(String hessianWebServiceAnnotationUrlPattern, Class<?> apiClazz, String urlPrefix) {
		final String urlPattern;
		if (!hessianWebServiceAnnotationUrlPattern.equals("")) {
			urlPattern = (urlPrefix + hessianWebServiceAnnotationUrlPattern).replaceFirst("//", "/");
		} else {
			urlPattern = urlPrefix + apiClazz.getSimpleName();
		}
		return urlPattern;
	}
}
