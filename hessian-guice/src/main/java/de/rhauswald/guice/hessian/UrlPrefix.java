package de.rhauswald.guice.hessian;

public class UrlPrefix {
	private final String prefix;

	private UrlPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return prefix;
	}

	@Override
	public String toString() {
		return "UrlPrefix{" +
				"prefix='" + prefix + '\'' +
				'}';
	}

	public static UrlPrefix none() {
		return forPrefix("/");
	}

	public static UrlPrefix forPrefix(String prefix) {
		if (prefix == null) {
			throw new NullPointerException("prefix must not be null");
		}
		if (!prefix.startsWith("/")) {
			throw new IllegalArgumentException("prefix must start with a '/'");
		}
		if (!prefix.endsWith("/")) {
			throw new IllegalArgumentException("prefix must end with a '/'");
		}
		final UrlPrefix urlPrefix = new UrlPrefix(prefix);
		return urlPrefix;
	}
}
