package de.rhauswald.guice.hessian.internal.stubservices.customurlpattern;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;

@HessianWebService(urlPattern = "/Custom")
public class StubApiCustomUrlPattern implements StubApi {
}
