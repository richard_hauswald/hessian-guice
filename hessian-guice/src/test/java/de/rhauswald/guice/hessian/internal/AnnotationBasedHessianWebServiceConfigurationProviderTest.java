package de.rhauswald.guice.hessian.internal;

import de.rhauswald.guice.hessian.UrlPrefix;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;
import de.rhauswald.guice.hessian.internal.stubservices.combinedapiclazzurlpatternnoautobind.StubApiCombinedApiClazzUrlPatternNoAutoBind;
import de.rhauswald.guice.hessian.internal.stubservices.combinednoautobindapiclazz.StubApiCombinedNoAutoBindApiClazz;
import de.rhauswald.guice.hessian.internal.stubservices.combinedurlpatternnoautobind.StubApiCombinedUrlPatternNoAutoBind;
import de.rhauswald.guice.hessian.internal.stubservices.customapiclass.StubApiCustomApiClass;
import de.rhauswald.guice.hessian.internal.stubservices.customurlpattern.StubApiCustomUrlPattern;
import de.rhauswald.guice.hessian.internal.stubservices.defaults.StubApiDefault;
import de.rhauswald.guice.hessian.internal.stubservices.noautobind.StubApiCustomNoAutoBind;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@RunWith(Parameterized.class)
public class AnnotationBasedHessianWebServiceConfigurationProviderTest {
	private final TestParameters testParameters;

	public AnnotationBasedHessianWebServiceConfigurationProviderTest(TestParameters testParameters) {
		this.testParameters = testParameters;
	}


	@Parameterized.Parameters(name = "testProvideConfigurations{0}")
	public static Collection configurations() {
		return Arrays.asList(new Object[][]{
				{new TestParameters(StubApi.class, UrlPrefix.none(), StubApiDefault.class, "/StubApi", true)},
				{new TestParameters(StubApi.class, UrlPrefix.forPrefix("/hessian/"), StubApiDefault.class, "/hessian/StubApi", true)},
				{new TestParameters(StubApi.class, UrlPrefix.none(), StubApiCustomUrlPattern.class, "/Custom", true)},
				{new TestParameters(StubApi.class, UrlPrefix.forPrefix("/hessian/"), StubApiCustomUrlPattern.class, "/hessian/Custom", true)},
				{new TestParameters(StubApi.class, UrlPrefix.none(), StubApiCustomApiClass.class, "/StubApi", true)},
				{new TestParameters(StubApi.class, UrlPrefix.forPrefix("/hessian/"), StubApiCustomApiClass.class, "/hessian/StubApi", true)},
				{new TestParameters(StubApi.class, UrlPrefix.none(), StubApiCustomNoAutoBind.class, "/StubApi", false)},
				{new TestParameters(StubApi.class, UrlPrefix.forPrefix("/hessian/"), StubApiCustomNoAutoBind.class, "/hessian/StubApi", false)},
				{new TestParameters(StubApi.class, UrlPrefix.none(), StubApiCombinedUrlPatternNoAutoBind.class, "/Custom", false)},
				{new TestParameters(StubApi.class, UrlPrefix.forPrefix("/hessian/"), StubApiCombinedUrlPatternNoAutoBind.class, "/hessian/Custom", false)},
				{new TestParameters(StubApi.class, UrlPrefix.none(), StubApiCombinedApiClazzUrlPatternNoAutoBind.class, "/Custom", false)},
				{new TestParameters(StubApi.class, UrlPrefix.forPrefix("/hessian/"), StubApiCombinedApiClazzUrlPatternNoAutoBind.class, "/hessian/Custom", false)},
				{new TestParameters(StubApi.class, UrlPrefix.none(), StubApiCombinedNoAutoBindApiClazz.class, "/StubApi", false)},
				{new TestParameters(StubApi.class, UrlPrefix.forPrefix("/hessian/"), StubApiCombinedNoAutoBindApiClazz.class, "/hessian/StubApi", false)},
		});
	}

	@Test
	public void testProvideConfigurations() throws Exception {
		final AnnotationBasedHessianWebServiceConfigurationProvider provider = sutForPrefix(testParameters);
		final String basePackage = testParameters.getAnnotatedApiImplClazz().getPackage().getName();

		Set<HessianWebServiceConfiguration> result = provider.provideConfigurations(basePackage);

		final Set<HessianWebServiceConfiguration> hessianWebServiceConfigurations = result;
		assertThat(hessianWebServiceConfigurations, hasSize(1));
		final HessianWebServiceConfiguration configuration = hessianWebServiceConfigurations.iterator().next();

		final HessianWebServiceConfiguration expected = expectedConfigurationFromTestParameters(testParameters);
		assertReflectionEquals(expected, configuration);
	}


	private AnnotationBasedHessianWebServiceConfigurationProvider sutForPrefix(TestParameters testParameters) {
		final UrlPrefix urlPrefix = testParameters.getUrlPrefix();
		return new AnnotationBasedHessianWebServiceConfigurationProvider(urlPrefix);
	}

	private HessianWebServiceConfiguration expectedConfigurationFromTestParameters(TestParameters testParameters) {
		final String expectedUrl = testParameters.getExpectedUrl();
		final Class<?> apiClazz = testParameters.getApiClazz();
		final Class<?> annotatedApiImplClazz = testParameters.getAnnotatedApiImplClazz();
		final boolean bindApiClazzToAnnotatedInstance = testParameters.isBindApiClazzToAnnotatedInstance();

		return new HessianWebServiceConfiguration(expectedUrl, apiClazz, annotatedApiImplClazz, bindApiClazzToAnnotatedInstance);
	}

	private static class TestParameters {
		private final Class<?> apiClazz;
		private final UrlPrefix urlPrefix;
		private final Class<?> annotatedApiImplClazz;
		private final String expectedUrl;
		private final boolean bindApiClazzToAnnotatedInstance;

		private TestParameters(Class<?> apiClazz, UrlPrefix urlPrefix, Class<?> annotatedApiImplClazz, String expectedUrl, boolean bindApiClazzToAnnotatedInstance) {
			this.apiClazz = apiClazz;
			this.urlPrefix = urlPrefix;
			this.annotatedApiImplClazz = annotatedApiImplClazz;
			this.expectedUrl = expectedUrl;
			this.bindApiClazzToAnnotatedInstance = bindApiClazzToAnnotatedInstance;
		}

		public Class<?> getApiClazz() {
			return apiClazz;
		}

		public UrlPrefix getUrlPrefix() {
			return urlPrefix;
		}

		public Class<?> getAnnotatedApiImplClazz() {
			return annotatedApiImplClazz;
		}

		public String getExpectedUrl() {
			return expectedUrl;
		}

		public boolean isBindApiClazzToAnnotatedInstance() {
			return bindApiClazzToAnnotatedInstance;
		}

		@Override
		public String toString() {
			return annotatedApiImplClazz.getSimpleName() + " - " + urlPrefix;
		}
	}
}
