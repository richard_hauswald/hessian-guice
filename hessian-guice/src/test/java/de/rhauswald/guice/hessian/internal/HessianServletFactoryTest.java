package de.rhauswald.guice.hessian.internal;

import com.caucho.hessian.server.HessianServlet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class HessianServletFactoryTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	private HessianServletFactory hessianServletFactory;

	@Before
	public void setUp() throws Exception {
		hessianServletFactory = new HessianServletFactory();
	}

	@Test
	public void testHessianServlet() throws Exception {
		final HessianServlet hessianServlet = hessianServletFactory.hessianServlet(ApiClazz.class, new ApiImpl());
		assertThat(hessianServlet, is(not(nullValue())));
		final Class expectedApiClass = ApiClazz.class;
		final Class apiClass = hessianServlet.getAPIClass();
		assertThat(apiClass, is(sameInstance(expectedApiClass)));
	}

	@Test
	public void testHessianServletComplaintsIfApiClassIsNoInterface() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("apiClazz must be an interface");

		hessianServletFactory.hessianServlet(AnotherClass.class, new AnotherClass());
	}

	@Test
	public void testHessianServletComplaintsIfDelegationInstanceDoesNotImplementApiClass() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("delegationInstance must implement apiClazz");

		hessianServletFactory.hessianServlet(ApiClazz.class, new AnotherClass());
	}

	private static interface ApiClazz {
	}

	private static class ApiImpl implements ApiClazz {
	}

	private static class AnotherClass {
	}
}
