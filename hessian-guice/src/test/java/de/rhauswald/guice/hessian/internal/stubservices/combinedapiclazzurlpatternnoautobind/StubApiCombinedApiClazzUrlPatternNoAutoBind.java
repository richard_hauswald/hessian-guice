package de.rhauswald.guice.hessian.internal.stubservices.combinedapiclazzurlpatternnoautobind;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;

@HessianWebService(apiClazz = StubApi.class, urlPattern = "/Custom", bindApiClazzToAnnotatedInstance = false)
public class StubApiCombinedApiClazzUrlPatternNoAutoBind implements Comparable, StubApi {
	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
