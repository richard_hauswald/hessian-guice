package de.rhauswald.guice.hessian.internal.stubservices.combinedurlpatternnoautobind;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;

@HessianWebService(urlPattern = "/Custom", bindApiClazzToAnnotatedInstance = false)
public class StubApiCombinedUrlPatternNoAutoBind implements StubApi {
}
