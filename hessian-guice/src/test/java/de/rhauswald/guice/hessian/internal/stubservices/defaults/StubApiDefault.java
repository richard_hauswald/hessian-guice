package de.rhauswald.guice.hessian.internal.stubservices.defaults;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;

@HessianWebService
public class StubApiDefault implements StubApi {
}
