package de.rhauswald.guice.hessian.internal.stubservices.combinednoautobindapiclazz;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;

@HessianWebService(apiClazz = StubApi.class, bindApiClazzToAnnotatedInstance = false)
public class StubApiCombinedNoAutoBindApiClazz implements Comparable, StubApi {
	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
