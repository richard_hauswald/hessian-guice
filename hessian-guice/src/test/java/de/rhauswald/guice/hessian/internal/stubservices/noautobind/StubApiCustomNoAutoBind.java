package de.rhauswald.guice.hessian.internal.stubservices.noautobind;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;

@HessianWebService(bindApiClazzToAnnotatedInstance = false)
public class StubApiCustomNoAutoBind implements StubApi {
}
