package de.rhauswald.guice.hessian.internal.stubservices.customapiclass;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;

@HessianWebService(apiClazz = StubApi.class)
public class StubApiCustomApiClass implements Comparable, StubApi {
	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
