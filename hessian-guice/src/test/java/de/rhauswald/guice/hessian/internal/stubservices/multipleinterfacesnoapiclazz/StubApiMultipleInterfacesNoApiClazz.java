package de.rhauswald.guice.hessian.internal.stubservices.multipleinterfacesnoapiclazz;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.internal.stubservices.StubApi;

@HessianWebService
public class StubApiMultipleInterfacesNoApiClazz implements Comparable, StubApi {
	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
