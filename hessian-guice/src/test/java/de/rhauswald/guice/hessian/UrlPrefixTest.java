package de.rhauswald.guice.hessian;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class UrlPrefixTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testNone() throws Exception {
		final UrlPrefix urlPrefix = UrlPrefix.none();
		assertThat(urlPrefix, is(not(nullValue())));
		assertThat(urlPrefix.getPrefix(), is(equalTo("/")));
	}

	@Test
	public void testForPrefix() throws Exception {
		final UrlPrefix urlPrefix = UrlPrefix.forPrefix("/hessian-web-services/");
		assertThat(urlPrefix, is(not(nullValue())));
		assertThat(urlPrefix.getPrefix(), is(equalTo("/hessian-web-services/")));
	}

	@Test
	public void testForPrefixRejectsPrefixWithoutLeadingSlash() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage(equalTo("prefix must start with a '/'"));

		UrlPrefix.forPrefix("hessian-web-services");
	}

	@Test
	public void testForPrefixRejectsPrefixWithoutTrailingSlash() throws Exception {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage(equalTo("prefix must end with a '/'"));

		UrlPrefix.forPrefix("/hessian-web-services");
	}

	@Test
	public void testForPrefixRejectsNullPrefix() throws Exception {
		expectedException.expect(NullPointerException.class);
		expectedException.expectMessage(equalTo("prefix must not be null"));

		UrlPrefix.forPrefix(null);
	}

	@Test
	public void testToString() throws Exception {
		final UrlPrefix urlPrefix = UrlPrefix.forPrefix("/hessian-web-services/");
		assertThat(urlPrefix.toString(), is(equalTo("UrlPrefix{prefix='/hessian-web-services/'}")));
	}
}
