package de.rhauswald.guice.hessian.it.impl.annotation;

import de.rhauswald.guice.hessian.it.api.TestWebService;
import de.rhauswald.guice.hessian.it.api.annotation.AnnotationDrivenMultipleInterfaces;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImplTest;

public class AnnotationDrivenMultipleInterfacesImplTest extends TestWebServiceImplTest{

	@Override
	protected String getRightOuterUrlPart() {
		return "/AnnotationDrivenMultipleInterfaces";
	}

	@Override
	protected Class<? extends TestWebService> getApiClazz() {
		return AnnotationDrivenMultipleInterfaces.class;
	}
}
