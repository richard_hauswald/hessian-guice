package de.rhauswald.guice.hessian.it.impl.manual;

import de.rhauswald.guice.hessian.it.api.TestWebService;
import de.rhauswald.guice.hessian.it.api.manual.ManualCustomKey;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImplTest;

public class ManualCustomKeyWebServiceImplTest extends TestWebServiceImplTest {

	@Override
	protected String getRightOuterUrlPart() {
		return "/manual/ManualCustomKey";
	}

	@Override
	protected Class<? extends TestWebService> getApiClazz() {
		return ManualCustomKey.class;
	}
}
