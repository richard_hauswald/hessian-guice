package de.rhauswald.guice.hessian.it.impl.annotation;

import de.rhauswald.guice.hessian.it.api.TestWebService;
import de.rhauswald.guice.hessian.it.api.annotation.AnnotationDrivenDefault;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImplTest;

public class AnnotationDrivenDefaultImplTest  extends TestWebServiceImplTest {

	@Override
	protected String getRightOuterUrlPart() {
		return "/AnnotationDrivenDefault";
	}

	@Override
	protected Class<? extends TestWebService> getApiClazz() {
		return AnnotationDrivenDefault.class;
	}
}
