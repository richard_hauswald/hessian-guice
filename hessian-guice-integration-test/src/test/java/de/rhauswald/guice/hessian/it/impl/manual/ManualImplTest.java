package de.rhauswald.guice.hessian.it.impl.manual;

import de.rhauswald.guice.hessian.it.api.TestWebService;
import de.rhauswald.guice.hessian.it.api.manual.Manual;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImplTest;

public class ManualImplTest extends TestWebServiceImplTest {

	@Override
	protected String getRightOuterUrlPart() {
		return "/manual/Manual";
	}

	@Override
	protected Class<? extends TestWebService> getApiClazz() {
		return Manual.class;
	}
}
