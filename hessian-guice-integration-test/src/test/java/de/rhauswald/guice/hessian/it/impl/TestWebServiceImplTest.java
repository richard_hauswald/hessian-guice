package de.rhauswald.guice.hessian.it.impl;

import com.caucho.hessian.client.HessianProxyFactory;
import de.rhauswald.guice.hessian.it.api.TestWebService;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public abstract class TestWebServiceImplTest {
	protected TestWebService testWebService;

	@Before
	public void setUp() throws Exception {
		final Class<? extends TestWebService> api = getApiClazz();
		final String urlName = "http://localhost:31337" + getRightOuterUrlPart();
		testWebService = (TestWebService) new HessianProxyFactory().create(api, urlName);
	}

	protected abstract String getRightOuterUrlPart();

	protected abstract Class<? extends TestWebService> getApiClazz();

	@Test
	public void testHelloWorld() throws Exception {
		final String helloWorld = testWebService.helloWorld("Chuck Norris");
		assertThat(helloWorld, is(equalTo("Hello Chuck Norris")));
	}
}
