package de.rhauswald.guice.hessian.it.impl.mixed;

import de.rhauswald.guice.hessian.it.api.TestWebService;
import de.rhauswald.guice.hessian.it.api.mixed.MixedManual;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImplTest;

public class MixedManualImplTest  extends TestWebServiceImplTest {

	@Override
	protected String getRightOuterUrlPart() {
		return "/mixed/Manual";
	}

	@Override
	protected Class<? extends TestWebService> getApiClazz() {
		return MixedManual.class;
	}
}
