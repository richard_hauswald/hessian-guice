package de.rhauswald.guice.hessian.it.impl.annotation;

import com.caucho.hessian.client.HessianProxyFactory;
import de.rhauswald.guice.hessian.it.api.TestWebService;
import de.rhauswald.guice.hessian.it.api.annotation.AnnotationDrivenNoAutoBinding;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AnnotationDrivenNoAutoBindingImplTest {
	private TestWebService testWebService;
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		final Class<AnnotationDrivenNoAutoBinding> api = AnnotationDrivenNoAutoBinding.class;
		final String urlName = "http://localhost:31337/AnnotationDrivenNoAutoBinding";
		testWebService = (TestWebService) new HessianProxyFactory().create(api, urlName);
	}

	@Test
	public void testHelloWorld() throws Exception {
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage(equalTo("The servers di framework is not configured properly"));
		testWebService.helloWorld("Chuck Norris");
	}
}
