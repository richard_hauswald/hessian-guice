package de.rhauswald.guice.hessian.it.impl.annotation;

import de.rhauswald.guice.hessian.it.api.TestWebService;
import de.rhauswald.guice.hessian.it.api.annotation.AnnotationDrivenCustomUrlPattern;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImplTest;

public class AnnotationDrivenCustomUrlPatternImplTest extends TestWebServiceImplTest {

	@Override
	protected String getRightOuterUrlPart() {
		return "/AnnotationDrivenCustomUrlPattern.hws";
	}

	@Override
	protected Class<? extends TestWebService> getApiClazz() {
		return AnnotationDrivenCustomUrlPattern.class;
	}
}
