package de.rhauswald.guice.hessian.it.impl.mixed;

import de.rhauswald.guice.hessian.it.api.TestWebService;
import de.rhauswald.guice.hessian.it.api.mixed.MixedAnnotationDriven;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImplTest;

public class MixedAnnotationDrivenImplTest extends TestWebServiceImplTest {

	@Override
	protected String getRightOuterUrlPart() {
		return "/mixed/MixedAnnotationDriven";
	}

	@Override
	protected Class<? extends TestWebService> getApiClazz() {
		return MixedAnnotationDriven.class;
	}
}
