package de.rhauswald.guice.hessian.it.impl;

import de.rhauswald.guice.hessian.it.api.TestWebService;

public abstract class TestWebServiceImpl implements TestWebService {
	@Override
	public String helloWorld(String greeting) {
		return "Hello " + greeting;
	}
}
