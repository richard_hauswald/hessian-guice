package de.rhauswald.guice.hessian.it.impl.mixed;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.it.api.mixed.MixedAnnotationDrivenNoAutoBinding;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImpl;

@HessianWebService(bindApiClazzToAnnotatedInstance = false)
public class MixedAnnotationDrivenNoAutoBindingImpl extends TestWebServiceImpl implements MixedAnnotationDrivenNoAutoBinding {
}
