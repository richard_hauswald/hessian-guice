package de.rhauswald.guice.hessian.it.impl.annotation;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.it.api.annotation.AnnotationDrivenMultipleInterfaces;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImpl;

@HessianWebService(apiClazz = AnnotationDrivenMultipleInterfaces.class)
public class AnnotationDrivenMultipleInterfacesImpl extends TestWebServiceImpl
		implements Comparable<AnnotationDrivenMultipleInterfacesImpl>, AnnotationDrivenMultipleInterfaces {
	@Override
	public int compareTo(AnnotationDrivenMultipleInterfacesImpl o) {
		return 0;
	}
}
