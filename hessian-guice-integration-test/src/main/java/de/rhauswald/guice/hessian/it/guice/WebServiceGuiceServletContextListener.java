package de.rhauswald.guice.hessian.it.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.servlet.GuiceServletContextListener;
import de.rhauswald.guice.hessian.HessianWebServicesModule;
import de.rhauswald.guice.hessian.UrlPrefix;
import de.rhauswald.guice.hessian.it.api.manual.Manual;
import de.rhauswald.guice.hessian.it.api.manual.ManualCustomKey;
import de.rhauswald.guice.hessian.it.api.mixed.MixedManual;
import de.rhauswald.guice.hessian.it.impl.manual.ManualCustomKeyWebServiceImpl;
import de.rhauswald.guice.hessian.it.impl.manual.ManualImpl;
import de.rhauswald.guice.hessian.it.impl.mixed.MixedManualImpl;

public class WebServiceGuiceServletContextListener extends GuiceServletContextListener {
	@Override
	protected Injector getInjector() {
		return Guice.createInjector(
				new HessianWebServicesModule("de.rhauswald.guice.hessian.it.impl.annotation"),
				new HessianWebServicesModule(){
					@Override
					protected void configureHessianWebServices() {
						bind(Manual.class).to(ManualImpl.class);
						serveHessianWebService(Manual.class).usingUrl("/manual/Manual");
						serveHessianWebService(ManualCustomKey.class, Key.get(ManualCustomKeyWebServiceImpl.class)).usingUrl("/manual/ManualCustomKey");
					}
				},
				new HessianWebServicesModule(UrlPrefix.forPrefix("/mixed/"), "de.rhauswald.guice.hessian.it.impl.mixed"){
					@Override
					protected void configureHessianWebServices() {
						bind(MixedManual.class).to(MixedManualImpl.class);
						serveHessianWebService(MixedManual.class).usingUrl("/mixed/Manual");
					}
				}
		);
	}
}
