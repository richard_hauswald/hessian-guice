package de.rhauswald.guice.hessian.it.impl.annotation;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.it.api.annotation.AnnotationDrivenDefault;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImpl;

@HessianWebService
public class AnnotationDrivenDefaultImpl extends TestWebServiceImpl implements AnnotationDrivenDefault {
}
