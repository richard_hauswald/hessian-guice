package de.rhauswald.guice.hessian.it.impl.mixed;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.it.api.mixed.MixedAnnotationDriven;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImpl;

@HessianWebService
public class MixedAnnotationDrivenImpl extends TestWebServiceImpl implements MixedAnnotationDriven {
}
