package de.rhauswald.guice.hessian.it.api.mixed;

import de.rhauswald.guice.hessian.it.api.TestWebService;

public interface MixedAnnotationDrivenNoAutoBinding extends TestWebService {
}
