package de.rhauswald.guice.hessian.it.impl.manual;

import de.rhauswald.guice.hessian.it.api.manual.ManualCustomKey;

public class ManualCustomKeyImpl implements ManualCustomKey {
	@Override
	public String helloWorld(String greeting) {
		return "I am not a web service";
	}
}
