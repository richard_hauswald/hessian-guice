package de.rhauswald.guice.hessian.it.api;

public interface TestWebService {
	String helloWorld(String greeting);
}
