package de.rhauswald.guice.hessian.it.impl.annotation;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.it.api.annotation.AnnotationDrivenCustomUrlPattern;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImpl;

@HessianWebService(urlPattern = "AnnotationDrivenCustomUrlPattern.hws")
public class AnnotationDrivenCustomUrlPatternImpl extends TestWebServiceImpl implements AnnotationDrivenCustomUrlPattern {
}
