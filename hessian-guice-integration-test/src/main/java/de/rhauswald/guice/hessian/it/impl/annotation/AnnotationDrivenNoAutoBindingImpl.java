package de.rhauswald.guice.hessian.it.impl.annotation;

import de.rhauswald.guice.hessian.HessianWebService;
import de.rhauswald.guice.hessian.it.api.annotation.AnnotationDrivenNoAutoBinding;
import de.rhauswald.guice.hessian.it.impl.TestWebServiceImpl;

@HessianWebService(bindApiClazzToAnnotatedInstance = false)
public class AnnotationDrivenNoAutoBindingImpl extends TestWebServiceImpl implements AnnotationDrivenNoAutoBinding {
}
