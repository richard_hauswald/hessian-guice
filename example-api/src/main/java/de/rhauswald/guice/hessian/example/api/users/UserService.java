package de.rhauswald.guice.hessian.example.api.users;

import java.util.List;

public interface UserService {
	List<DtoUser> list();
}
