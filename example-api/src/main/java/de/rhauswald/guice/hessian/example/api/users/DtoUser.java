package de.rhauswald.guice.hessian.example.api.users;

import java.io.Serializable;

public class DtoUser implements Serializable {
	private String name;
	private String email;

	public DtoUser() {
	}

	public DtoUser(String name, String email) {
		this.name = name;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "DtoUser{" +
				"name='" + name + '\'' +
				", email='" + email + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DtoUser dtoUser = (DtoUser) o;

		if (email != null ? !email.equals(dtoUser.email) : dtoUser.email != null) return false;
		if (name != null ? !name.equals(dtoUser.name) : dtoUser.name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (email != null ? email.hashCode() : 0);
		return result;
	}
}
