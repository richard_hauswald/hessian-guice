package de.rhauswald.guice.hessian.example.api.espressobeans;

import java.util.List;

public interface EspressoBeanService {
	List<DtoEspressoBean> list();
}
