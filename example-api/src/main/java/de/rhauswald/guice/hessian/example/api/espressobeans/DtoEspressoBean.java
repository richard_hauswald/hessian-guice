package de.rhauswald.guice.hessian.example.api.espressobeans;

import java.io.Serializable;

public class DtoEspressoBean implements Serializable {
	private String name;

	public DtoEspressoBean() {
	}

	public DtoEspressoBean(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "DtoEspressoBean{" +
				"name='" + name + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DtoEspressoBean that = (DtoEspressoBean) o;

		if (name != null ? !name.equals(that.name) : that.name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return name != null ? name.hashCode() : 0;
	}
}
